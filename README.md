## Testing

1. Install `pytorch` and openai `gym`
2. `python setup.py develop`
3. `python test.py`

## Useful Gym Resources

- https://github.com/openai/gym/tree/master/gym/spaces

- https://github.com/MartinThoma/banana-gym/blob/master/gym_banana/envs/banana_env.py

- https://github.com/MattChanTK/gym-maze/blob/master/gym_maze/envs/maze_env.py

- https://github.com/openai/gym/blob/master/gym/core.py

- https://github.com/openai/gym-soccer/blob/master/gym_soccer/envs/soccer_env.py

- https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

- https://pytorch.org/tutorials/intermediate/mario_rl_tutorial.html

- Spreadsheet of cards: https://docs.google.com/spreadsheets/d/1yqB6Q8CDI7elEjbLmwX6056aBp0xQDQ-C1NtpqVfuI8/edit#gid=0
