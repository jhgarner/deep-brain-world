import gym
import re
from gym import error, spaces, utils
from gym.spaces import Discrete, MultiDiscrete, MultiBinary, Dict, Tuple
from gym.utils import seeding
import csv
import pathlib
import os
import random
from collections import defaultdict

NUM_CARDS = 52

# 0 represents empty
NUM_WITH_EMPTY = NUM_CARDS + 1

class Card:
    def __init__(self, qty, name, faction, card_type, cost, text, defense):
        self.qty = int(qty)
        self.name = name
        self.faction = faction
        self.card_type = card_type
        if cost == '':
            self.cost = 0
        else:
            self.cost = int(cost)
        self.text = text
        self.defense = int(defense.split()[0]) if len(defense) > 0 else 0
        self.is_mandatory = 'outpost' in defense


        self.play = []
        self.scrap = []
        self.ally = []

        for line in text.split('\n'):
            location = self.play
            if str.startswith(line, "Ally:"):
                line = line[6:]
                location = self.ally
            if str.startswith(line, "Scrap:"):
                line = line[7:]
                location = self.scrap
            m = re.match(r'Add (\d+) Combat', line)
            if m:
                def add_combat(state: "StarRealms", amount=int(m.group(1))):
                    player = state.player()
                    player.damage += int(amount)
                location.append(add_combat)
            m = re.match(r'Add (\d+) Trade', line)
            if m:
                def add_money(state: "StarRealms", amount=int(m.group(1))):
                    player = state.player()
                    player.money += int(amount)
                location.append(add_money)
            m = re.match(r'You may scrap a card in the trade row', line)
            if m:
                def scrap_trade(state: "StarRealms"):
                    player = state.player()
                    player.must_scrap_trade_row += 1
                location.append(scrap_trade)
            m = re.match(r'Draw a card', line)
            if m:
                def draw_card(state: "StarRealms"):
                    state.player().draw()
                location.append(draw_card)
            m = re.match(r'Acquire any ship without paying its cost and put it on top of your deck', line)
            if m:
                def acquire(state: "StarRealms"):
                    state.player().free_cards += 1
                location.append(acquire)
            m = re.match(r'Destroy target base', line)
            if m:
                def destroy_base(state: "StarRealms"):
                    state.player().can_destroy_free += 1
                location.append(destroy_base)


    def pprint(self):
        print(self.name, self.faction, self.card_type, self.cost, sep=', ')
        print(self.text)
        print(self.defense)


DIR = ''
ALL_CARDS: dict[str, Card] = {}
with open(os.path.join(DIR, 'starrealms.csv')) as csvfile:
    cardreader = csv.reader(csvfile)

    for card in cardreader:
        if card[0] == 'Core Set':
            ALL_CARDS[card[2]] = Card(*card[1:])


# ====== Action space stuff ======

"""
0-10 = play index of card in hand
10-26 = buy card from trade row
"""


class PlayerState:
    def __init__(self):
        self.reset()

    def reset(self):
        self.hand = defaultdict(int)
        self.deck = []
        self.discard = defaultdict(int)
        self.num_discard = 0
        self.played = defaultdict(int)
        self.money = 0
        self.damage = 0
        self.bases = defaultdict(int)
        self.mandatory_bases = defaultdict(int)
        self.health = 50
        self.must_scrap_hand = 0
        self.must_scrap_discard = 0
        self.free_cards = 0
        self.can_destroy_free = 0
        self.must_scrap_either = 0
        self.must_scrap_trade_row = 0

    def draw(self):
        if len(self.deck) == 0:
            self.shuffle()
        self.hand[self.deck[-1]] += 1
        self.deck = self.deck[:-1]

    def shuffle(self):
        for name, qty in self.discard.items():
            for i in range(qty):
                self.deck.append(name)
        self.discard = defaultdict(int)
        random.shuffle(self.deck)


class Action:
    def __init__(self):
        pass

class StarRealms():
    #metadata = {'render.modes': ['human']}

    # observation_space =

    def __init__(self):
        self.available_cards: list[str] = []
        self.trade_row: defaultdict[str, int] = defaultdict(int)
        self.player1_state = PlayerState()
        self.player2_state = PlayerState()
        self.player1_turn = True

    def step(self, action):
        pass

    def reset(self):
        self.player1_state.reset()
        self.player2_state.reset()
        for name, card in ALL_CARDS.items():
            if name in ['Scout', 'Viper', 'Explorer']:
                continue
            for _ in range(card.qty):
                self.available_cards.append(name)
        random.shuffle(self.available_cards)
        for _ in range(5):
            top = self.available_cards.pop()
            self.trade_row[top] += 1
        for player in [self.player1_state, self.player2_state]:
            player.deck = ['Scout']*8 + ['Viper']*2
            random.shuffle(player.deck)
            for _ in range(5):
                player.draw()

    def render(self, mode='human'):
        pass

    def close(self):
        pass

    def player(self):
        return self.player1_state if self.player1_turn else self.player2_state

    def opponent(self):
        return self.player2_state if self.player1_turn else self.player1_state

    def scrap(self, card_to_scrap: str, from_hand: bool):
        player: PlayerState = self.player()
        if from_hand:
            player.hand[card_to_scrap] -= 1
        else:
            player.discard[card_to_scrap] -= 1

    def play_card(self, card_to_play: str):
        player = self.player()
        player.played[card_to_play] += 1
        player.hand[card_to_play] -= 1

    def discard(self, card_to_discard: str):
        player = self.player()
        player.hand[card_to_discard] -= 1
        player.discard[card_to_discard] += 1

    def buy(self, card_to_buy: str):
        player = self.player()
        player.discard[card_to_buy] += 1
        if card_to_buy != "Explorer":
            self.trade_row[card_to_buy] -= 1
        player.money -= ALL_CARDS[card_to_buy].cost

    def damage(self):
        player = self.player()
        target = self.opponent()
        target.health -= player.damage

    def destroy(self, to_destroy: str):
        player = self.opponent()
        if ALL_CARDS[to_destroy].is_mandatory:
            player.mandatory_bases[to_destroy] -= 1
        else:
            player.bases[to_destroy] -= 1
        player.damage -= ALL_CARDS[to_destroy].defense
