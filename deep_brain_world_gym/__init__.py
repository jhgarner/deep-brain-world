from gym.envs.registration import register

register(
    id='starrealms_env-v0',
    entry_point='deep_brain_world_gym.envs:StarRealms',
)
