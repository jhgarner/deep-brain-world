{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
}:
with pkgs;
let
  pythonEnv = python3.withPackages (ps: with ps; [
    pytorch-bin
    gym
  ]);

in mkShell {
  buildInputs = [
    niv
    pythonEnv
  ];
}
